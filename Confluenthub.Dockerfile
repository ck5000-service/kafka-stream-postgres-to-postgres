FROM confluentinc/cp-kafka-connect-base:6.0.0

RUN confluent-hub install --no-prompt confluentinc/kafka-connect-jdbc:10.0.0 \
&& confluent-hub install --no-prompt debezium/debezium-connector-postgresql:1.2.2 \
&& confluent-hub install --no-prompt debezium/debezium-connector-mysql:1.3.1
